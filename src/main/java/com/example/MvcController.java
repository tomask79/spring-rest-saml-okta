package com.example;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by HP on 16.4.2017.
 */
@RestController
public class MvcController {

    @GetMapping("/")
    public String getStringBySaml() {
        final StringBuffer output = new StringBuffer(255);
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.isAuthenticated()) {
            output.append("Principal: "+authentication.getPrincipal());
            output.append("Authorities: "+authentication.getAuthorities());
            output.append("User object: "+authentication.getPrincipal().getClass());
        }
        return output.toString();
    }
}

# SSO using Spring SAML extension with Okta or ADFS as Identity Provider #

I assembled example of howto create REST endpoint secured with [Spring Security SAML](http://projects.spring.io/spring-security-saml/) and [Okta ](https://www.okta.com/) or [ADFS ](https://en.wikipedia.org/wiki/Active_Directory_Federation_Services) as Identity Providers. Let's go through the steps of howto configure the things to run it.

## SAML with Okta as IdP ##

Okta has excellent tutorial of howto set things up: [Okta and Spring Boot Saml](http://developer.okta.com/blog/2017/03/16/spring-boot-saml). Definitely check it out. Anyway. when creating application which should be secured by SAML you've got two options:

* Use the **org.springframework.security.extensions.saml2.config.SAMLConfigurer** which wires up many complicated things for you. All you need to configure is the service provider (your application) and identity provider's metadata. Which you can download to the offline storage and refer it as file or you can set URL to it. See [Configuration of Idp Metadata](http://docs.spring.io/spring-security-saml/docs/1.0.x/reference/html/chapter-quick-start.html#quick-start-idp-metadata).

* Your second option is to wire up everything manually. Which is the way I'd highly recommend to you, because you'll understand many things of how SAML works under the hood.
Definitely start with samlFilter method. This repository contains this second option. To run it against Okta, just change the link to your Okta's IdP metadata, like:

```
#!python

security:
  saml2:
      metadata-url: https://dev-257435.oktapreview.com/app/exka6jwaqkW1ORVe40h7/sso/saml/metadata
```
      

## SAML with ADFS as IdP ##

This repository contains working example against ADFS. I succefully tested it in our company. Let's just summarize things which I had to do:

* I let the Spring framework to generate ServiceProvider metadata. I downloaded them from my app at https://localhost:8443/saml/metadata and provided them to our company ADFS admin.

* When I first ran the application against the ADFS I crashed with: "**org.apache.xml.security.encryption.XMLEncryptionException: Illegal key size**" To fix this issue I had to install SunJCE extended cryptography library for my JDK from http://www.oracle.com/technetwork/java/javase/downloads/index.html. See this link from Stackoverflow: Spring [SAML ADFS: java.security.InvalidKeyException](https://stackoverflow.com/questions/18893568/spring-saml-adfs-java-security-invalidkeyexception/18948613)

* When wiring** org.springframework.security.saml.metadata.ExtendedMetadataDelegate** bean I switched off the metadata trust check, because I didn't want to mess up with the ADFS certificate installation to my keystore in order to download and load the ADFS metadata. But in the production this settings isn't probably desirable. 

* Then with the ADFS admin in our company we followed the following instructions from the doc:

1. Store content of the generated SP Metadata to a document metadata.xml and upload it to the AD FS server
2. In AD FS 2.0 Management Console select "Add Relying Party Trust"
3. Select "Import data about the relying party from a file" and select the metadata.xml file created earlier. Select Next
4. The wizard may complain that some content of metadata is not supported. You can safely ignore this warning
5. Continue with the wizard. On the "Ready to Add Trust" make sure that tab endpoints contains multiple endpoint values. If not, verify that your metadata was generated with HTTPS protocol URLs
6. Leave "Open the Edit Claim Rules dialog" checkbox checked and finish the wizard
7. Select "Add Rule", choose "Send LDAP Attributes as Claims" and press Next
8. Add NameID as "Claim rule name", choose "Active Directory" as Attribute store, choose "SAM-Account-Name" as LDAP Attribute and "Name ID" as "Outgoing claim type", finish the wizard and confirm the claim rules window, in ADFS 3.0 you might need to configure the Name ID as a Pass Through claim
9. Open the provider by double-clicking it, select tab Advanced and change "Secure hash algorithm" to SHA-1

And that's it.

## Testing the ADFS demo ##

* git clone <this repo>
* Set the link to your ADFS metadata in application.yml
* mvn clean install
* Start the application in your IDE via the DemoApplication class...(SP metatadata should be already installed at ADFS)
* Verify that application downloaded ADFS metadata(output should be like):

2017-05-25 10:26:15.530  INFO 10284 --- [ost-startStop-1] .s.m.p.AbstractReloadingMetadataProvider :** New metadata succesfully loaded for 'https://adfsserver/federationmetadata/2007-06/federationmetadata.xml'**
2017-05-25 10:26:15.544  INFO 10284 --- [ost-startStop-1] .s.m.p.AbstractReloadingMetadataProvider : Next refresh cycle for metadata provider 'https://adfsserver/federationmetadata/2007-06/federationmetadata.xml' will occur on '2017-05-25T11:26:15.308Z' ('2017-05-25T13:26:15.308+02:00' local time)

* hit the https://localhost:8443 and log in to your domain.
* In my case REST endpoint returned:

Principal: org.springframework.security.core.userdetails.User@117e5cb8: Username: Tomas.Kloucek; Password: [PROTECTED]; Enabled: true; AccountNonExpired: true;  credentialsNonExpired: true; AccountNonLocked: true; Granted Authorities: ROLE_USERAuthorities: [ROLE_USER]User object: class org.springframework.security.core.userdetails.User

regards

Tomas